#
# This class installs an IIS site
#
# Parameters:
#
#   (string) site_name
#              - Name of the site
#
#   (string) site_path
#               - Absolute path to content
#
#   (int)    site_port
#               - Listen port
#
#   (string) site_ip
#               - Listen IP address
#
#   (string) site_host_header
#               - Header hostname
#
#   (string) site_associated_pool
#               - Associated application pool
#
#   (enum)   ensure
#              - present, running, installed, stopped or absent
#
# Actions:
#   Configure IIS site
#
# Requires:
#   opentable/iis
#
define windows_webserver::pool (
  $enable_32_bit,
  $managed_runtime_version,
  $managed_pipeline_mode,
  $sites,
  $ensure ) {

  include ::windows_webserver::params

  validate_bool($enable_32_bit)
  validate_re($managed_runtime_version, ['^(v2\.0|v4\.0)$'])
  validate_re($managed_pipeline_mode, ['^(Integrated|Classic)$'])
  validate_re($ensure, '^(present|installed|absent|purged)$')
  validate_hash($sites)

  ::iis::manage_app_pool { DefaultAppPool:
    ensure                    => absent
  }

  ::iis::manage_app_pool { $name:
    enable_32_bit             => $enable_32_bit,
    managed_runtime_version   => $managed_runtime_version,
    managed_pipeline_mode     => $managed_pipeline_mode,
    ensure                    => $ensure
  }

  $sites_default = {
    app_pool => $name,
  }

  create_resources('windows_webserver::site', $sites, $sites_default)
}