#
# This class installs an IIS site
#
# Parameters:
#
#   (string) name
#              - Name is virtual_application_name
#
#   (string) site_path
#               - Absolute path to content
#
#   (int)    site_port
#               - Listen port
#
#   (string) site_ip
#               - Listen IP address
#
#   (string) site_host_header
#               - Header hostname
#
#   (string) site_associated_pool
#               - Associated application pool
#
#   (enum)   ensure
#              - present, running, installed, stopped or absent
#
# Actions:
#   Configure IIS site
#
# Requires:
#   opentable/iis
#

define windows_webserver::virtual_app (
  $site_name,
  $site_path,
  $site_source,
  $app_pool,
  $ensure) {

  include ::windows_webserver::params

  validate_string($name)
  validate_string($site_name)
  validate_string($site_path)
  validate_string($site_source)
  validate_string($app_pool)
  validate_re($ensure, '^(present|installed|absent|purged)$')

  ::iis::manage_virtual_application { $name:
    site_name                 => $site_name,
    site_path                 => $site_path,
    app_pool                  => $app_pool,
    ensure                    => $ensure
  }
  ->
  # copy test into place
  file { "${site_path}/default.htm":
    source             => "${site_source}/default.htm",
    source_permissions => ignore,
  }
  ->
  file { "${site_path}/style.css":
    source             => "${site_source}/style.css",
    source_permissions => ignore,
  }

}