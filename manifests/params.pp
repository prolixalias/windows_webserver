#
# This class installs an IIS application pool, site and virtual apps
#
# Parameters:
#
#   app_pools parameters:
#      app_pool_name                       - Name of the application pool
#      app_pool_enable_32_bit              - Determine if 32-bit available
#      app_pool_dotnet_version             - Specify dotnet version
#
#   sites parameters:
#      site_name                           - Name of the site
#      site_path                           - Absolute path to content
#      site_port                           - Listen port
#      site_ip_address                     - Listen address
#      site_host_header                    - Site header
#      site_associated_pool                - Associated application pool
#
#   virtual_application parameters
#      virtual_app_name                    - Name of virtual application
#      virtual_site_name                   - Associated site
#      virtual_site_path                   - Absolute path to content
#      virtual_associated_pool             - Associated application pool
#
# Actions:
#
#   Configure application pool
#   Configure site
#   Configure virtual application
#
# Requires:
#
#   opentable/iis
#
class windows_webserver::params {
}
