#
# This class installs an IIS site
#
# Parameters:
#
#   (string) name
#               - The name is equiv to site_name
#
#   (string) site_path
#               - Absolute path to content
#
#   (int)    site_port
#               - Listen port
#
#   (string) site_ip
#               - Listen IP address
#
#   (string) site_host_header
#               - Header hostname
#
#   (string) site_associated_pool
#               - Associated application pool
#
#   (enum)   ensure
#              - present, running, installed, stopped or absent
#
# Actions:
#   Configure IIS site
#
# Requires:
#   opentable/iis
#
define windows_webserver::site (
  $site_path,
  $site_source,
  $app_pool,
  $host_header,
  $port,
  $ip_address,
  $ssl,
  $virtual_apps,
  $ensure) {

  include ::windows_webserver::params

  validate_string($name)
  validate_string($site_source)
  validate_string($app_pool)
  validate_string($host_header)
  validate_string($host_header)
  validate_string($port)
  validate_string($ip_address)
  validate_string($ssl)
  validate_hash($virtual_apps)
  validate_re($ensure, '^(present|installed|absent|purged)$')

  ::iis::manage_site { "Default Web Site":
    site_path         => 'C:\inetpub\wwwroot',
    app_pool          => 'DefaultAppPool',
    ensure            => absent
  }

  #
  # site_name comes from name variable
  #
  ::iis::manage_site { $name:
    site_path          => $site_path,
    app_pool           => $app_pool,
    host_header        => $host_header,
    site_name          => $name,
    port               => $port,
    ip_address         => $ip_address,
    ssl                => $ssl,
    ensure             => $ensure
  }
  ->
  # copy test into place
  file { "${site_path}/default.htm":
    source             => "${site_source}/default.htm",
    source_permissions => ignore,
  } ->
  file { "${site_path}/style.css":
    source             => "${site_source}/style.css",
    source_permissions => ignore,
  }

  $filename = regsubst($site_source, '.*/(.*)', '\1')

  if ! defined(Staging::File[$filename]) {
    staging::file { $filename:
      source => $site_source,
    }
  }

  staging::extract { "${name}-${filename}":
    source             => "${::staging::path}/iis/${filename}",
    target             => $site_path,
    require            => Staging::File[$filename],
    strip              => $_strip,
  }

  $virtual_apps_default = {
    site_name          => $name,
    app_pool           => $app_pool,
  }

  create_resources('windows_webserver::virtual_app', $virtual_apps, $virtual_apps_default)

}